var _WM_APP_PROPERTIES = {
  "activeTheme" : "air-crew",
  "defaultLanguage" : "en",
  "displayName" : "AirCrew_Management",
  "homePage" : "Main",
  "name" : "AirCrew_Management",
  "platformType" : "MOBILE",
  "securityEnabled" : "false",
  "supportedLanguages" : "en",
  "type" : "APPLICATION",
  "version" : "1.0"
};