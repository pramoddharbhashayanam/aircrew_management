Application.$controller("Assigned_FlightsPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
        if (localStorage.getItem("toastFlag") == "true") {
            $scope.Variables.assignSuccessToast.notify();

            //last step
            localStorage.setItem("toastFlag", false);
            localStorage.setItem("flightId", "");

        }
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };


    $scope.assigned_flightsonSuccess = function(variable, data) {
        $scope.Widgets.label1.caption = data.content.length + " ASSIGNED FLIGHTS";
    };


    $scope.livelist1Tap = function($event, $isolateScope) {
        $scope.Variables.selected_flight_details.dataSet.PAGE = 'Assigned';
    };

}]);