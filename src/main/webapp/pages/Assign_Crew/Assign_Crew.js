Application.$controller("Assign_CrewPageController", ["$scope", function($scope) {
    "use strict";

    /* perform any action with the variables inside this block(on-page-load) */
    $scope.onPageVariablesReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. $scope.Variables.staticVariable1.getData()
         */
    };

    /* perform any action with widgets inside this block */
    $scope.onPageReady = function() {
        /*
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. $scope.Widgets.byId(), $scope.Widgets.byName()or access widgets by $scope.Widgets.widgetName
         */
    };




    var fo1set = false;
    var fo2set = false;
    var cc7set = false;
    var cc8set = false;
    var cc1set = false;
    var cc2set = false;
    var cc3set = false;
    var cc4set = false;
    var cc5set = false;
    var cc6set = false;



    $scope.button2_1Tap = function($event, $isolateScope) {
        //$scope.Variables.Get_flight_details.update();


        var captaincontent = $scope.Variables.captain_list.dataSet.dataValue;
        if (captaincontent.length == undefined) {
            captaincontent = [captaincontent];
        }
        captaincontent.forEach(function(arrayItem) {
            if ($scope.Variables.Get_flight_details.dataSet.content[0].CAPTAIN == 0) {
                $scope.Variables.update_Captain.setInput('captainId', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data1', arrayItem.USER_ID);
            }
        });
        if (captaincontent.length != 0) {
            $scope.Variables.update_Captain.update();
            $scope.Variables.update_status_for_user.update();
        }


        var focontent = $scope.Variables.fo_list.dataSet.dataValue;
        if (focontent.length == undefined) {
            focontent = [focontent];
        }
        focontent.forEach(function(arrayItem) {
            if ($scope.Variables.Get_flight_details.dataSet.content[0].FIRST_OFFICER_1 === 0 && !fo1set) {
                $scope.Variables.update_fo.setInput('FIRST_OFFICER_1', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data1', arrayItem.USER_ID);
                fo1set = true;
            } else
            if ($scope.Variables.Get_flight_details.dataSet.content[0].FIRST_OFFICER_2 === 0 && !fo2set) {
                $scope.Variables.update_fo.setInput('FIRST_OFFICER_2', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data2', arrayItem.USER_ID);
                fo2set = true;
            }
        });
        if (focontent.length != 0) {
            $scope.Variables.update_fo.update();
            $scope.Variables.update_status_for_user.update();
        }




        var cccontent = $scope.Variables.cc_list.dataSet.dataValue;
        $scope.Variables.Get_flight_details.dataSet.content[0];
        if (cccontent.length == undefined) {
            cccontent = [cccontent];
        }
        cccontent.forEach(function(arrayItem) {
            if ($scope.Variables.Get_flight_details.dataSet.content[0].CC1 == 0 && !cc1set) {
                $scope.Variables.update_cc.setInput('cc1', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data1', arrayItem.USER_ID);
                cc1set = true;
            } else
            if ($scope.Variables.Get_flight_details.dataSet.content[0].CC2 == 0 && !cc2set) {
                $scope.Variables.update_cc.setInput('cc2', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data2', arrayItem.USER_ID);
                cc2set = true;
            } else
            if ($scope.Variables.Get_flight_details.dataSet.content[0].CC3 == 0 && !cc3set) {
                $scope.Variables.update_cc.setInput('cc3', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data3', arrayItem.USER_ID);
                cc3set = true;
            } else
            if ($scope.Variables.Get_flight_details.dataSet.content[0].CC4 == 0 && !cc4set) {
                $scope.Variables.update_cc.setInput('cc4', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data4', arrayItem.USER_ID);
                cc4set = true;
            } else
            if ($scope.Variables.Get_flight_details.dataSet.content[0].CC5 == 0 && !cc5set) {
                $scope.Variables.update_cc.setInput('cc5', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data5', arrayItem.USER_ID);
                cc5set = true;
            } else
            if ($scope.Variables.Get_flight_details.dataSet.content[0].CC6 == 0 && !cc6set) {
                $scope.Variables.update_cc.setInput('cc6', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data6', arrayItem.USER_ID);
                cc6set = true;
            } else
            if ($scope.Variables.Get_flight_details.dataSet.content[0].CC7 == 0 && !cc7set) {
                $scope.Variables.update_cc.setInput('cc7', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data7', arrayItem.USER_ID);
                cc7set = true;
            } else
            if ($scope.Variables.Get_flight_details.dataSet.content[0].CC8 == 0 && !cc8set) {
                $scope.Variables.update_cc.setInput('cc8', arrayItem.USER_ID);
                $scope.Variables.update_status_for_user.setInput('data8', arrayItem.USER_ID);
                cc8set = true;
            }
        });
        if (cccontent.length != 0) {
            $scope.Variables.update_cc.update();
            $scope.Variables.update_status_for_user.update();
        }
    };




    $scope.getPendingCountonSuccess = function(variable, data) {

        if (data.content[0].PendingCount != 0) {
            // go to main and display toast
            localStorage.setItem("flightId", variable.dataBinding.flightId);
            localStorage.setItem("toastFlag", true);
            $scope.Variables.goToPage_Main.navigate();
        } else {

            // go to assigned flights and display toast
            localStorage.setItem("flightId", variable.dataBinding.flightId);
            localStorage.setItem("toastFlag", true);
            $scope.Variables.goToPage_Assigned_Flights.navigate();
        }

    };



}]);