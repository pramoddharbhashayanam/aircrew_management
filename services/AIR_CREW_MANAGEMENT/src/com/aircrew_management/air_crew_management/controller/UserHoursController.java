/*Generated by WaveMaker Studio*/
package com.aircrew_management.air_crew_management.controller;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.sql.Date;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;
import com.wavemaker.tools.api.core.annotations.WMAccessVisibility;
import com.wavemaker.tools.api.core.models.AccessSpecifier;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import com.wordnik.swagger.annotations.ApiParam;

import com.aircrew_management.air_crew_management.UserHours;
import com.aircrew_management.air_crew_management.UserHoursId;
import com.aircrew_management.air_crew_management.service.UserHoursService;


/**
 * Controller object for domain model class UserHours.
 * @see UserHours
 */
@RestController("AIR_CREW_MANAGEMENT.UserHoursController")
@Api(value = "UserHoursController", description = "Exposes APIs to work with UserHours resource.")
@RequestMapping("/AIR_CREW_MANAGEMENT/UserHours")
public class UserHoursController {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserHoursController.class);

    @Autowired
	@Qualifier("AIR_CREW_MANAGEMENT.UserHoursService")
	private UserHoursService userHoursService;

	@ApiOperation(value = "Creates a new UserHours instance.")
    @RequestMapping(method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public UserHours createUserHours(@RequestBody UserHours userHours) {
		LOGGER.debug("Create UserHours with information: {}" , userHours);

		userHours = userHoursService.create(userHours);
		LOGGER.debug("Created UserHours with information: {}" , userHours);

	    return userHours;
	}

@ApiOperation(value = "Returns the UserHours instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public UserHours getUserHours(@RequestParam("day") Date day,@RequestParam("userId") Integer userId) throws EntityNotFoundException {

        UserHoursId userhoursId = new UserHoursId();
        userhoursId.setDay(day);
        userhoursId.setUserId(userId);

        LOGGER.debug("Getting UserHours with id: {}" , userhoursId);
        UserHours userHours = userHoursService.getById(userhoursId);
        LOGGER.debug("UserHours details with id: {}" , userHours);

        return userHours;
    }



    @ApiOperation(value = "Updates the UserHours instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.PUT)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public UserHours editUserHours(@RequestParam("day") Date day,@RequestParam("userId") Integer userId, @RequestBody UserHours userHours) throws EntityNotFoundException {

        userHours.setDay(day);
        userHours.setUserId(userId);

        LOGGER.debug("UserHours details with id is updated with: {}" , userHours);

        return userHoursService.update(userHours);
    }


    @ApiOperation(value = "Deletes the UserHours instance associated with the given composite-id.")
    @RequestMapping(value = "/composite-id", method = RequestMethod.DELETE)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public boolean deleteUserHours(@RequestParam("day") Date day,@RequestParam("userId") Integer userId) throws EntityNotFoundException {

        UserHoursId userhoursId = new UserHoursId();
        userhoursId.setDay(day);
        userhoursId.setUserId(userId);

        LOGGER.debug("Deleting UserHours with id: {}" , userhoursId);
        UserHours userHours = userHoursService.delete(userhoursId);

        return userHours != null;
    }


    /**
     * @deprecated Use {@link #findUserHours(String, Pageable)} instead.
     */
    @Deprecated
    @ApiOperation(value = "Returns the list of UserHours instances matching the search criteria.")
    @RequestMapping(value = "/search", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<UserHours> searchUserHoursByQueryFilters( Pageable pageable, @RequestBody QueryFilter[] queryFilters) {
        LOGGER.debug("Rendering UserHours list");
        return userHoursService.findAll(queryFilters, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of UserHours instances matching the optional query (q) request param. If there is no query provided, it returns all the instances. Pagination & Sorting parameters such as page& size, sort can be sent as request parameters. The sort value should be a comma separated list of field names & optional sort order to sort the data on. eg: field1 asc, field2 desc etc ")
    @RequestMapping(method = RequestMethod.GET)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<UserHours> findUserHours(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering UserHours list");
        return userHoursService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns the paginated list of UserHours instances matching the optional query (q) request param. This API should be used only if the query string is too big to fit in GET request with request param. The request has to made in application/x-www-form-urlencoded format.")
    @RequestMapping(value="/filter", method = RequestMethod.POST, consumes= "application/x-www-form-urlencoded")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Page<UserHours> filterUserHours(@ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
        LOGGER.debug("Rendering UserHours list");
        return userHoursService.findAll(query, pageable);
    }

    @ApiOperation(value = "Returns downloadable file for the data matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
    @RequestMapping(value = "/export/{exportType}", method = {RequestMethod.GET,  RequestMethod.POST}, produces = "application/octet-stream")
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
    public Downloadable exportUserHours(@PathVariable("exportType") ExportType exportType, @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query, Pageable pageable) {
         return userHoursService.export(exportType, query, pageable);
    }

	@ApiOperation(value = "Returns the total count of UserHours instances matching the optional query (q) request param. If query string is too big to fit in GET request's query param, use POST method with application/x-www-form-urlencoded format.")
	@RequestMapping(value = "/count", method = {RequestMethod.GET, RequestMethod.POST})
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Long countUserHours( @ApiParam("conditions to filter the results") @RequestParam(value = "q", required = false) String query) {
		LOGGER.debug("counting UserHours");
		return userHoursService.count(query);
	}

    @ApiOperation(value = "Returns aggregated result with given aggregation info")
	@RequestMapping(value = "/aggregations", method = RequestMethod.POST)
    @WMAccessVisibility(value = AccessSpecifier.APP_ONLY)
	public Page<Map<String, Object>> getUserHoursAggregatedValues(@RequestBody AggregationInfo aggregationInfo, Pageable pageable) {
        LOGGER.debug("Fetching aggregated results for {}", aggregationInfo);
        return userHoursService.getAggregatedValues(aggregationInfo, pageable);
    }


    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service UserHoursService instance
	 */
	protected void setUserHoursService(UserHoursService service) {
		this.userHoursService = service;
	}

}

