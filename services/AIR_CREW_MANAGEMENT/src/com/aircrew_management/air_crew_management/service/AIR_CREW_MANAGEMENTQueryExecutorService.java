/*Generated by WaveMaker Studio*/

package com.aircrew_management.air_crew_management.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.file.model.Downloadable;

import com.aircrew_management.air_crew_management.models.query.*;

public interface AIR_CREW_MANAGEMENTQueryExecutorService {

    Page<AssignedFlightsResponse> executeAssigned_flights(Pageable pageable);

    Downloadable exportAssigned_flights(ExportType exportType, Pageable pageable);

    Page<UnassignedFlightsDetailsResponse> executeUnassigned_flights_details(Integer managerId, Pageable pageable);

    Downloadable exportUnassigned_flights_details(ExportType exportType, Integer managerId, Pageable pageable);

    Page<GetPendingCountForFlightIdResponse> executeGetPendingCountForFlightId(String flightId, Pageable pageable);

    Downloadable exportGetPendingCountForFlightId(ExportType exportType, String flightId, Pageable pageable);

    Integer executeUpdate_cc(UpdateCcRequest updateCcRequest);

    Integer executeUpdate_status_for_user(UpdateStatusForUserRequest updateStatusForUserRequest);

    Integer executeUpdate_fo(UpdateFoRequest updateFoRequest);

    Integer executeUpdate_user_day(UpdateUserDayRequest updateUserDayRequest);

    Integer executeAvaliablequery(AvaliablequeryRequest avaliablequeryRequest);

    Page<GetCrewPicUrlResponse> executeGet_crew_pic_url(String data, Pageable pageable);

    Downloadable exportGet_crew_pic_url(ExportType exportType, String data, Pageable pageable);

    Page<GetCrewDetailsResponse> executeGet_crew_details(String data, Pageable pageable);

    Downloadable exportGet_crew_details(ExportType exportType, String data, Pageable pageable);

    Page<AvaialbleUsersResponse> executeAvaialbleUSers(String fid, Pageable pageable);

    Downloadable exportAvaialbleUSers(ExportType exportType, String fid, Pageable pageable);

    Integer executeResetQuery(ResetQueryRequest resetQueryRequest);

    Page<AvalbleUsersWithHoursResponse> executeAvalbleUSersWithHours(String flightSource, String rolee, Pageable pageable);

    Downloadable exportAvalbleUSersWithHours(ExportType exportType, String flightSource, String rolee, Pageable pageable);

    Page<GetFlightDetailsResponse> executeGet_flight_details(String data, Pageable pageable);

    Downloadable exportGet_flight_details(ExportType exportType, String data, Pageable pageable);

    Integer executeUpdateCaptain(UpdateCaptainRequest updateCaptainRequest);

}


