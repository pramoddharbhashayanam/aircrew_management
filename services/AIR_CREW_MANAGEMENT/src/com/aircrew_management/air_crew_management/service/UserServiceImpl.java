/*Generated by WaveMaker Studio*/
package com.aircrew_management.air_crew_management.service;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Lazy;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.annotation.Validated;

import com.wavemaker.runtime.data.dao.WMGenericDao;
import com.wavemaker.runtime.data.exception.EntityNotFoundException;
import com.wavemaker.runtime.data.export.ExportType;
import com.wavemaker.runtime.data.expression.QueryFilter;
import com.wavemaker.runtime.data.model.AggregationInfo;
import com.wavemaker.runtime.file.model.Downloadable;

import com.aircrew_management.air_crew_management.FlightDay;
import com.aircrew_management.air_crew_management.User;
import com.aircrew_management.air_crew_management.UserDay;
import com.aircrew_management.air_crew_management.UserHours;


/**
 * ServiceImpl object for domain model class User.
 *
 * @see User
 */
@Service("AIR_CREW_MANAGEMENT.UserService")
@Validated
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserServiceImpl.class);

    @Lazy
    @Autowired
	@Qualifier("AIR_CREW_MANAGEMENT.UserDayService")
	private UserDayService userDayService;

    @Lazy
    @Autowired
	@Qualifier("AIR_CREW_MANAGEMENT.UserHoursService")
	private UserHoursService userHoursService;

    @Lazy
    @Autowired
	@Qualifier("AIR_CREW_MANAGEMENT.FlightDayService")
	private FlightDayService flightDayService;

    @Autowired
    @Qualifier("AIR_CREW_MANAGEMENT.UserDao")
    private WMGenericDao<User, Integer> wmGenericDao;

    public void setWMGenericDao(WMGenericDao<User, Integer> wmGenericDao) {
        this.wmGenericDao = wmGenericDao;
    }

    @Transactional(value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
	public User create(User user) {
        LOGGER.debug("Creating a new User with information: {}", user);
        return this.wmGenericDao.create(user);
    }

	@Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public User getById(Integer userIdInstance) throws EntityNotFoundException {
        LOGGER.debug("Finding User by id: {}", userIdInstance);
        User user = this.wmGenericDao.findById(userIdInstance);
        if (user == null){
            LOGGER.debug("No User found with id: {}", userIdInstance);
            throw new EntityNotFoundException(String.valueOf(userIdInstance));
        }
        return user;
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public User findById(Integer userIdInstance) {
        LOGGER.debug("Finding User by id: {}", userIdInstance);
        return this.wmGenericDao.findById(userIdInstance);
    }


	@Transactional(rollbackFor = EntityNotFoundException.class, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public User update(User user) throws EntityNotFoundException {
        LOGGER.debug("Updating User with information: {}", user);

        if(user.getFlightDays() != null) {
            for(FlightDay _flightDay : user.getFlightDays()) {
                _flightDay.setUser(user);
            }
        }
        if(user.getUserDays() != null) {
            for(UserDay _userDay : user.getUserDays()) {
                _userDay.setUser(user);
            }
        }
        if(user.getUserHourses() != null) {
            for(UserHours _userHours : user.getUserHourses()) {
                _userHours.setUser(user);
            }
        }

        this.wmGenericDao.update(user);

        Integer userIdInstance = user.getUserId();

        return this.wmGenericDao.findById(userIdInstance);
    }

    @Transactional(value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public User delete(Integer userIdInstance) throws EntityNotFoundException {
        LOGGER.debug("Deleting User with id: {}", userIdInstance);
        User deleted = this.wmGenericDao.findById(userIdInstance);
        if (deleted == null) {
            LOGGER.debug("No User found with id: {}", userIdInstance);
            throw new EntityNotFoundException(String.valueOf(userIdInstance));
        }
        this.wmGenericDao.delete(deleted);
        return deleted;
    }

	@Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public Page<User> findAll(QueryFilter[] queryFilters, Pageable pageable) {
        LOGGER.debug("Finding all Users");
        return this.wmGenericDao.search(queryFilters, pageable);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
    public Page<User> findAll(String query, Pageable pageable) {
        LOGGER.debug("Finding all Users");
        return this.wmGenericDao.searchByQuery(query, pageable);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
    public Downloadable export(ExportType exportType, String query, Pageable pageable) {
        LOGGER.debug("exporting data in the service AIR_CREW_MANAGEMENT for table User to {} format", exportType);
        return this.wmGenericDao.export(exportType, query, pageable);
    }

	@Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
	public long count(String query) {
        return this.wmGenericDao.count(query);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
	@Override
    public Page<Map<String, Object>> getAggregatedValues(AggregationInfo aggregationInfo, Pageable pageable) {
        return this.wmGenericDao.getAggregatedValues(aggregationInfo, pageable);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
    public Page<FlightDay> findAssociatedFlightDays(Integer userId, Pageable pageable) {
        LOGGER.debug("Fetching all associated flightDays");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("user.userId = '" + userId + "'");

        return flightDayService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
    public Page<UserDay> findAssociatedUserDays(Integer userId, Pageable pageable) {
        LOGGER.debug("Fetching all associated userDays");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("user.userId = '" + userId + "'");

        return userDayService.findAll(queryBuilder.toString(), pageable);
    }

    @Transactional(readOnly = true, value = "AIR_CREW_MANAGEMENTTransactionManager")
    @Override
    public Page<UserHours> findAssociatedUserHourses(Integer userId, Pageable pageable) {
        LOGGER.debug("Fetching all associated userHourses");

        StringBuilder queryBuilder = new StringBuilder();
        queryBuilder.append("user.userId = '" + userId + "'");

        return userHoursService.findAll(queryBuilder.toString(), pageable);
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service UserDayService instance
	 */
	protected void setUserDayService(UserDayService service) {
        this.userDayService = service;
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service UserHoursService instance
	 */
	protected void setUserHoursService(UserHoursService service) {
        this.userHoursService = service;
    }

    /**
	 * This setter method should only be used by unit tests
	 *
	 * @param service FlightDayService instance
	 */
	protected void setFlightDayService(FlightDayService service) {
        this.flightDayService = service;
    }

}

