/*Generated by WaveMaker Studio*/
package com.aircrew_management.air_crew_management.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wavemaker.runtime.data.annotations.ColumnAlias;

public class GetPendingCountForFlightIdResponse implements Serializable {


    @JsonProperty("PendingCount")
    @ColumnAlias("PendingCount")
    private BigDecimal pendingCount;

    public BigDecimal getPendingCount() {
        return this.pendingCount;
    }

    public void setPendingCount(BigDecimal pendingCount) {
        this.pendingCount = pendingCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetPendingCountForFlightIdResponse)) return false;
        final GetPendingCountForFlightIdResponse getPendingCountForFlightIdResponse = (GetPendingCountForFlightIdResponse) o;
        return Objects.equals(getPendingCount(), getPendingCountForFlightIdResponse.getPendingCount());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPendingCount());
    }
}
