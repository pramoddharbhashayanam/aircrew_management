/*Generated by WaveMaker Studio*/
package com.aircrew_management.air_crew_management.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.wavemaker.runtime.data.annotations.ColumnAlias;

public class GetFlightDetailsResponse implements Serializable {


    @JsonProperty("FIRST_OFFICER_1")
    @ColumnAlias("FIRST_OFFICER_1")
    private Integer firstOfficer1;

    @JsonProperty("FIRST_OFFICER_2")
    @ColumnAlias("FIRST_OFFICER_2")
    private Integer firstOfficer2;

    @JsonProperty("CC8")
    @ColumnAlias("CC8")
    private Integer cc8;

    @JsonProperty("FLIGHT_ID")
    @ColumnAlias("FLIGHT_ID")
    private String flightId;

    @JsonProperty("CREW_MANAGER")
    @ColumnAlias("CREW_MANAGER")
    private Integer crewManager;

    @JsonProperty("DAY")
    @ColumnAlias("DAY")
    private Date day;

    @JsonProperty("CC3")
    @ColumnAlias("CC3")
    private Integer cc3;

    @JsonProperty("CC2")
    @ColumnAlias("CC2")
    private Integer cc2;

    @JsonProperty("CC1")
    @ColumnAlias("CC1")
    private Integer cc1;

    @JsonProperty("CC7")
    @ColumnAlias("CC7")
    private Integer cc7;

    @JsonProperty("CC6")
    @ColumnAlias("CC6")
    private Integer cc6;

    @JsonProperty("CC5")
    @ColumnAlias("CC5")
    private Integer cc5;

    @JsonProperty("CC4")
    @ColumnAlias("CC4")
    private Integer cc4;

    @JsonProperty("CAPTAIN")
    @ColumnAlias("CAPTAIN")
    private Integer captain;

    public Integer getFirstOfficer1() {
        return this.firstOfficer1;
    }

    public void setFirstOfficer1(Integer firstOfficer1) {
        this.firstOfficer1 = firstOfficer1;
    }

    public Integer getFirstOfficer2() {
        return this.firstOfficer2;
    }

    public void setFirstOfficer2(Integer firstOfficer2) {
        this.firstOfficer2 = firstOfficer2;
    }

    public Integer getCc8() {
        return this.cc8;
    }

    public void setCc8(Integer cc8) {
        this.cc8 = cc8;
    }

    public String getFlightId() {
        return this.flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public Integer getCrewManager() {
        return this.crewManager;
    }

    public void setCrewManager(Integer crewManager) {
        this.crewManager = crewManager;
    }

    public Date getDay() {
        return this.day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Integer getCc3() {
        return this.cc3;
    }

    public void setCc3(Integer cc3) {
        this.cc3 = cc3;
    }

    public Integer getCc2() {
        return this.cc2;
    }

    public void setCc2(Integer cc2) {
        this.cc2 = cc2;
    }

    public Integer getCc1() {
        return this.cc1;
    }

    public void setCc1(Integer cc1) {
        this.cc1 = cc1;
    }

    public Integer getCc7() {
        return this.cc7;
    }

    public void setCc7(Integer cc7) {
        this.cc7 = cc7;
    }

    public Integer getCc6() {
        return this.cc6;
    }

    public void setCc6(Integer cc6) {
        this.cc6 = cc6;
    }

    public Integer getCc5() {
        return this.cc5;
    }

    public void setCc5(Integer cc5) {
        this.cc5 = cc5;
    }

    public Integer getCc4() {
        return this.cc4;
    }

    public void setCc4(Integer cc4) {
        this.cc4 = cc4;
    }

    public Integer getCaptain() {
        return this.captain;
    }

    public void setCaptain(Integer captain) {
        this.captain = captain;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GetFlightDetailsResponse)) return false;
        final GetFlightDetailsResponse getFlightDetailsResponse = (GetFlightDetailsResponse) o;
        return Objects.equals(getFirstOfficer1(), getFlightDetailsResponse.getFirstOfficer1()) &&
                Objects.equals(getFirstOfficer2(), getFlightDetailsResponse.getFirstOfficer2()) &&
                Objects.equals(getCc8(), getFlightDetailsResponse.getCc8()) &&
                Objects.equals(getFlightId(), getFlightDetailsResponse.getFlightId()) &&
                Objects.equals(getCrewManager(), getFlightDetailsResponse.getCrewManager()) &&
                Objects.equals(getDay(), getFlightDetailsResponse.getDay()) &&
                Objects.equals(getCc3(), getFlightDetailsResponse.getCc3()) &&
                Objects.equals(getCc2(), getFlightDetailsResponse.getCc2()) &&
                Objects.equals(getCc1(), getFlightDetailsResponse.getCc1()) &&
                Objects.equals(getCc7(), getFlightDetailsResponse.getCc7()) &&
                Objects.equals(getCc6(), getFlightDetailsResponse.getCc6()) &&
                Objects.equals(getCc5(), getFlightDetailsResponse.getCc5()) &&
                Objects.equals(getCc4(), getFlightDetailsResponse.getCc4()) &&
                Objects.equals(getCaptain(), getFlightDetailsResponse.getCaptain());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirstOfficer1(),
                getFirstOfficer2(),
                getCc8(),
                getFlightId(),
                getCrewManager(),
                getDay(),
                getCc3(),
                getCc2(),
                getCc1(),
                getCc7(),
                getCc6(),
                getCc5(),
                getCc4(),
                getCaptain());
    }
}
