/*Generated by WaveMaker Studio*/
package com.aircrew_management.air_crew_management.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateCaptainRequest implements Serializable {


    @JsonProperty("captainId")
    private Integer captainId;

    @JsonProperty("flightId")
    private String flightId;

    @JsonProperty("day")
    private Date day;

    @JsonProperty("crew_mgr")
    private Integer crewMgr;

    public Integer getCaptainId() {
        return this.captainId;
    }

    public void setCaptainId(Integer captainId) {
        this.captainId = captainId;
    }

    public String getFlightId() {
        return this.flightId;
    }

    public void setFlightId(String flightId) {
        this.flightId = flightId;
    }

    public Date getDay() {
        return this.day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Integer getCrewMgr() {
        return this.crewMgr;
    }

    public void setCrewMgr(Integer crewMgr) {
        this.crewMgr = crewMgr;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdateCaptainRequest)) return false;
        final UpdateCaptainRequest updateCaptainRequest = (UpdateCaptainRequest) o;
        return Objects.equals(getCaptainId(), updateCaptainRequest.getCaptainId()) &&
                Objects.equals(getFlightId(), updateCaptainRequest.getFlightId()) &&
                Objects.equals(getDay(), updateCaptainRequest.getDay()) &&
                Objects.equals(getCrewMgr(), updateCaptainRequest.getCrewMgr());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCaptainId(),
                getFlightId(),
                getDay(),
                getCrewMgr());
    }
}
