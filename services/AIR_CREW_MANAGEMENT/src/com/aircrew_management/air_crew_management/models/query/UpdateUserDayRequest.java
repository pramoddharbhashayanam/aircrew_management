/*Generated by WaveMaker Studio*/
package com.aircrew_management.air_crew_management.models.query;

/*This is a Studio Managed File. DO NOT EDIT THIS FILE. Your changes may be reverted by Studio.*/


import java.io.Serializable;
import java.sql.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

public class UpdateUserDayRequest implements Serializable {


    @JsonProperty("flightid")
    private String flightid;

    @JsonProperty("day")
    private Date day;

    @JsonProperty("userid")
    private Integer userid;

    public String getFlightid() {
        return this.flightid;
    }

    public void setFlightid(String flightid) {
        this.flightid = flightid;
    }

    public Date getDay() {
        return this.day;
    }

    public void setDay(Date day) {
        this.day = day;
    }

    public Integer getUserid() {
        return this.userid;
    }

    public void setUserid(Integer userid) {
        this.userid = userid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdateUserDayRequest)) return false;
        final UpdateUserDayRequest updateUserDayRequest = (UpdateUserDayRequest) o;
        return Objects.equals(getFlightid(), updateUserDayRequest.getFlightid()) &&
                Objects.equals(getDay(), updateUserDayRequest.getDay()) &&
                Objects.equals(getUserid(), updateUserDayRequest.getUserid());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFlightid(),
                getDay(),
                getUserid());
    }
}
